
class MyThread extends Thread{
    @Override
    public void run() {
       // super.run();
        System.out.println(" hello  i am from mythread!! ");
    }
}

class ThreadTwo implements Runnable{

    @Override
    public void run() {
        System.out.println(" This is from runnable interface !! ");
    }
}

public class Main {
    // extends Thread class
    public static void main(String[] args) {
        Thread thread1 = new Thread(){
            @Override
            public void run() {
                System.out.println(" This is first thread !!");
            }
        };
        thread1.start();
        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(" this is the second thread ");
            }
        });
        thread2.start();
    }
}
